package com.bmpos.plugin.bmpos_plugin;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.content.Context;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.RemoteException;
import android.text.InputFilter;
import android.text.InputType;
import android.widget.EditText;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.EventChannel;

import com.wizarpos.aidl.ICloudPay;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.HashMap;
import java.util.Map;


/** BmposPlugin */
public class BmposPlugin implements FlutterPlugin, MethodCallHandler, EventChannel.StreamHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private EventChannel eventChannel;
  private EventChannel.EventSink onEventListener;
  private ICloudPay mWizarPayment;
  final ServiceConnection mConnPayment = new PaymentConnection();
  public static Context _Context;
  private String param, response;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "bmpos_plugin");
    channel.setMethodCallHandler(this);
    _Context = flutterPluginBinding.getApplicationContext();

    eventChannel = new EventChannel(flutterPluginBinding.getBinaryMessenger(), "bmpos_event_channel");
    eventChannel.setStreamHandler(this);

  }

  @Override
  public void onListen(Object arguments, EventChannel.EventSink events) {
    this.onEventListener = events;
  }

  @Override
  public void onCancel(Object arguments) {
    this.onEventListener = null;
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    param = "";
    response = "";

    switch (call.method) {
      case "bind":
        Log.d("BMPOS", "Start Bind");
        this.bindPaymentRouter();
        result.success("");
        break;
      case "un_bind":
        Log.d("BMPOS", "Start Unbind");
        this.unbindPaymentRouter();
        result.success("");
        break;
      case "cancel_transaction":
        try {
          Log.d("BMPOS", "Cancel Transaction");
          mWizarPayment.cancelTransaction();
          result.success("");
          break;
        } catch (RemoteException e) {
          throw new RuntimeException(e);
        }
      case "get_pos_info":
      case "void_sale":
      case "set_param":
      case "login":
      case "settle":
      case "print_last":
      case "sale":
        Log.d("BMPOS", "RECEIVE METHOD " + call.method);
        HashMap<String, Object> dataFromFlutter = call.arguments();
        if (dataFromFlutter != null) {
          param = dataFromFlutter.toString();
          if (mWizarPayment == null) {
            response = "Please [Bind] First!";
            param = "";
          } else if(!mWizarPayment.asBinder().isBinderAlive()){
            response = "Please [Bind] First!";
            param = "";
          } else if (null == param) {
            response = "Call parameter failed!";
          }
          if (response == "") {
            createAsyncTask(call.method).thenAccept(response -> {
//        // Xử lý kết quả ở đây
              this.response = response;
              updateReceivedResponse();
              Log.d("BMPOS RESULT", "RESULT RESPONSE " + response);
            });
            return;
          }
          Log.d("BMPOS", "Param DATA" + param);
          updateReceivedResponse();
        }
        result.success("");
        break;
      default:
        result.notImplemented();
    }

  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    this.unbindPaymentRouter();
    channel.setMethodCallHandler(null);
  }

  class PaymentConnection implements ServiceConnection {
    @Override
    public void onServiceConnected(ComponentName compName, IBinder binder) {
      Log.d("onServiceConnected", "compName: " + compName);
      mWizarPayment = ICloudPay.Stub.asInterface(binder);
      updateReceivedResponse("Connect Success!");
    }

    @Override
    public void onServiceDisconnected(ComponentName compName) {
      Log.d("onServiceDisconnected", "compName: " + compName);
      mWizarPayment = null;
      param = "";
      updateReceivedResponse("Disconnect Success!");
    }
  };

  private void bindPaymentRouter() {
    if (mWizarPayment == null) {
      Intent intent = new Intent("com.wizarpos.aidl.ICloudPay");
      intent.setPackage("com.wizarpos.napas");
      _Context.bindService(intent, mConnPayment, Context.BIND_AUTO_CREATE);
    }
  }
  private void unbindPaymentRouter() {
    if (mWizarPayment != null) {
      Log.d("receive method", "service call unbind");
      _Context.unbindService(mConnPayment);
      mWizarPayment = null;
      updateReceivedResponse();
    }
  }

  public void updateReceivedResponse(String response) {
    this.response = response;
    updateReceivedResponse();
  }

  public void updateReceivedResponse() {
//    Log.d("BMPOS", "Param " + param + "\n" + response);
    Map<String, Object> map = new HashMap<>();
    map.put("result", response);
    map.put("param", param);
    map.put("is_bind", mWizarPayment != null);
    sendDataToFlutter(map);
  }

  void sendDataToFlutter(Object data) {
    // Get a handler that can be used to post to the main thread
    Handler mainHandler = new Handler(Looper.getMainLooper());
    Runnable myRunnable = () -> {
      onEventListener.success(data);
    };
    mainHandler.post(myRunnable);
  }

  private CompletableFuture<String> createAsyncTask(String parameter) {
    CompletableFuture<String> future = new CompletableFuture<>();

    Executor executor = Executors.newSingleThreadExecutor();
    executor.execute(() -> {
      try {
        // Thực hiện các tác vụ bất đồng bộ với tham số được truyền vào ở đây
        String result = "Skipped";
        switch (parameter) {
          case "sale":
          case "void_sale":
            result = mWizarPayment.transact(this.param); break;
          case "print_last":
            result = mWizarPayment.printLast(this.param); break;
          case "settle":
            result = mWizarPayment.settle(this.param); break;
          case "set_param":
            result = mWizarPayment.setParams(this.param); break;
          case "login":
            result = mWizarPayment.login(); break;
          case "get_pos_info":
            result = mWizarPayment.getPOSInfo(this.param); break;
        }
        future.complete(result);
      } catch (Exception e) {
        future.completeExceptionally(e);
      }
    });

    return future.thenApply(result -> {
      // Khi tác vụ hoàn thành, kết quả sẽ được trả về ở đây
      // Bạn có thể thực hiện các hành động sau khi nhận được kết quả ở đây
      return result;
    });
  }

}
