import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'bmpos_plugin_method_channel.dart';

abstract class BmposPluginPlatform extends PlatformInterface {
  /// Constructs a BmposPluginPlatform.
  BmposPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static BmposPluginPlatform _instance = MethodChannelBmposPlugin();

  /// The default instance of [BmposPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelBmposPlugin].
  static BmposPluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [BmposPluginPlatform] when
  /// they register themselves.
  static set instance(BmposPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  // Future<String?> getPlatformVersion() {
  //   throw UnimplementedError('platformVersion() has not been implemented.');
  // }

  Future<void> bind() {
    throw UnimplementedError('bind() has not been implemented.');
  }

  Future<void> unBind() {
    throw UnimplementedError('unBind() has not been implemented.');
  }

  Future<void> setParam({required Map<String, dynamic> data}) {
    throw UnimplementedError('setParam() has not been implemented.');
  }

  Future<void> login({required Map<String, dynamic> data}) {
    throw UnimplementedError('login() has not been implemented.');
  }

  Future<void> settle({required Map<String, dynamic> data}) {
    throw UnimplementedError('settle() has not been implemented.');
  }

  Future<void> printLast({required Map<String, dynamic> data}) {
    throw UnimplementedError('printLast() has not been implemented.');
  }

  Future<void> sale({required Map<String, dynamic> data}) {
    throw UnimplementedError('sale() has not been implemented.');
  }

  Future<void> voidSale({required Map<String, dynamic> data}) {
    throw UnimplementedError('voidSale() has not been implemented.');
  }

  Future<void> cancelTransaction() {
    throw UnimplementedError('cancelTransaction() has not been implemented.');
  }
  
  Future<void> getPOSInfo({required Map<String, dynamic> data}) {
    throw UnimplementedError('getPOSInfo() has not been implemented.');
  }

  Future<void> transact() {
    throw UnimplementedError('transact() has not been implemented.');
  }
  

}
