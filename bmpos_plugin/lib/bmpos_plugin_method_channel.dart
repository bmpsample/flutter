import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'bmpos_plugin_platform_interface.dart';

/// An implementation of [BmposPluginPlatform] that uses method channels.
class MethodChannelBmposPlugin extends BmposPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('bmpos_plugin');

  // @override
  // Future<String?> getPlatformVersion() async {
  //   final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
  //   return version;
  // }

  @override
  Future<void> bind()  {
    return methodChannel.invokeMethod("bind");
  }

  @override
  Future<void> unBind() async {
    methodChannel.invokeMethod("un_bind");
  }

  @override
  Future<void> getPOSInfo({required Map<String, dynamic> data}) async {
    // TODO: implement getPOSInfo
    methodChannel.invokeMethod("get_pos_info", data);
  }

  @override
  Future<void> settle({required Map<String, dynamic> data}) async {
    methodChannel.invokeMethod("settle", data);
  }

@override
  Future<void> transact() async {
  final transact = await methodChannel.invokeMethod("get_transact_info");
    return transact;
  }

  @override
  Future<void> sale({required Map<String, dynamic> data}) async {
    methodChannel.invokeMethod("sale", data);
  }

  @override
  Future<void> voidSale({required Map<String, dynamic> data}) async {
    methodChannel.invokeMethod("void_sale", data);
  }

  @override
  Future<void> cancelTransaction() async {
    final result = await methodChannel.invokeMethod("cancel_transaction");
    return result;
  }

  @override
  Future<void> printLast({required Map<String, dynamic> data}) async {
    methodChannel.invokeMethod("print_last", data);
  }

  @override
  Future<void> login({required Map<String, dynamic> data}) async {
    methodChannel.invokeMethod("login", data);
  }

  @override
  Future<void> setParam({required Map<String, dynamic> data}) async {
    methodChannel.invokeMethod("set_param", data);
  }

}
