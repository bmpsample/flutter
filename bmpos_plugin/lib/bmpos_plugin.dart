
import 'bmpos_plugin_platform_interface.dart';
import 'package:flutter/services.dart';
import 'dart:async';

class BMPOSPlugin {
  static const _eventChannel = EventChannel('bmpos_event_channel');
  
  Stream<dynamic> onEvent() {
    return _eventChannel.receiveBroadcastStream();
  }

  // Future<String?> getPlatformVersion() {
  //   return BmposPluginPlatform.instance.getPlatformVersion();
  // }

  Future<void> bind() {
    return BmposPluginPlatform.instance.bind();
  }

  Future<void> unBind() {
    return BmposPluginPlatform.instance.unBind();
  }
  
  Future<void> setParam({required Map<String, dynamic> data}) {
    return BmposPluginPlatform.instance.setParam(data: data);
  }

  Future<void> login({required Map<String, dynamic> data}) {
    return BmposPluginPlatform.instance.login(data: data);
  }
  
  Future<void> settle({required Map<String, dynamic> data}) {
    return BmposPluginPlatform.instance.settle(data: data);
  }

  Future<void> printLast({required Map<String, dynamic> data}) {
    return BmposPluginPlatform.instance.printLast(data: data);
  }

  Future<void> sale({required Map<String, dynamic> data}) {
    return BmposPluginPlatform.instance.sale(data: data);
  }
  
  Future<void> voidSale({required Map<String, dynamic> data}) {
    return BmposPluginPlatform.instance.voidSale(data: data);
  }

  Future<void> cancelTransaction() {
    return BmposPluginPlatform.instance.cancelTransaction();
  }

  Future<void> getPOSInfo({required Map<String, dynamic> data}) async {
    return BmposPluginPlatform.instance.getPOSInfo(data: data);
  }

}

class BMAction {
  static const String bind = "bind";
  static const String unBind = "un_bind";
  static const String setParam = "set_param";
  static const String login = "login";
  static const String settle = "settle";
  static const String printLast = "print_last";
  static const String sale = "sale";
  static const String voidSale = "void_sale";
  static const String cancel = "cancel";
  static const String getPosInfor = "get_pos_infor";
}
