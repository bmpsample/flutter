#import "BmposPlugin.h"
#if __has_include(<bmpos_plugin/bmpos_plugin-Swift.h>)
#import <bmpos_plugin/bmpos_plugin-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "bmpos_plugin-Swift.h"
#endif

@implementation BmposPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBmposPlugin registerWithRegistrar:registrar];
}
@end
