import 'package:flutter/material.dart';

import 'package:bmpos_plugin/bmpos_plugin.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late TextEditingController _controller;

  final BMPOSPlugin _bmposPlugin = BMPOSPlugin();

  String _method = '';
  String _param = '';
  String _result = '';

  List<MerchantAction> actionList = [];
  bool _isBind = false;


  String TEST_IP1 = "113.164.14.80";
  int TEST_PORT1 = 11251;
  String TEST_IP2 = "113.164.14.80";
  int TEST_PORT2 = 11251;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();

    _bmposPlugin.onEvent().listen((event) {
      String result = event['result'] ?? "";
      String param = event['param'] ?? "";
      bool isBind = event['is_bind'] ?? false;
      setState(() {
        _result = result;
        _param = param;
        _isBind = isBind;
      });
    });

    actionList = [
      MerchantAction('Bind', BMAction.bind),
      MerchantAction('UnBind', BMAction.unBind),
      MerchantAction('Set Param', BMAction.setParam),
      MerchantAction('Login', BMAction.login),
      MerchantAction('Settle', BMAction.settle),
      MerchantAction('Print Last', BMAction.printLast),
      MerchantAction('Sale', BMAction.sale),
      MerchantAction('Void Sale', BMAction.voidSale),
      MerchantAction('POS Infor', BMAction.getPosInfor),
      MerchantAction('Cancel', BMAction.cancel),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('BM\'s Merchant'),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    const Text('Bind status'),
                    const SizedBox(width: 16,),
                    Icon(Icons.online_prediction_rounded, size: 32, color: _isBind ? Colors.green : Colors.grey,)
                  ],
                ),
              ),
              GridView.builder(
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(8),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: 8,
                      crossAxisSpacing: 8,
                      childAspectRatio: 2),
                  itemCount: actionList.length,
                  itemBuilder: (ctx, index) {
                    return InkWell(
                        onTap: () async {
                          _method = actionList[index].title;
                          handleClickAction(actionList[index].key, ctx);
                        },
                        child: Container(
                            height: 30,
                            color: Colors.black12,
                            child: Center(child: Text(actionList[index].title))));
                  }),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Method: '),
                    Expanded(
                        child: Text(
                          _method,
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.left,
                        )),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Param: '),
                    Expanded(
                        child: Text(
                          _param,
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.left,
                        )),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Result: '),
                    Expanded(
                        child: Text(
                          _result,
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.left,
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///Thực hiện gọi sang BMPos Plugin
  void handleClickAction(String action, BuildContext ctx) {
    Map<String, dynamic> param = setParamForAction(action);
    _param = param.toString();
    try {
      // platformVersion = await _bmposPlugin.getPlatformVersion() ?? 'Unknown platform version';
      switch (action) {
        case BMAction.bind:
          _bmposPlugin.bind();
          break;
        case BMAction.unBind:
          _bmposPlugin.unBind();
          break;
        case BMAction.setParam:
          _bmposPlugin.setParam(data: param);
          break;
        case BMAction.login:
          _bmposPlugin.login(data: param);
          break;
        case BMAction.settle:
          _bmposPlugin.settle(data: param);
          break;
        case BMAction.printLast:
          _bmposPlugin.printLast(data: param);
          break;
        case BMAction.sale:
          _bmposPlugin.sale(data: param);
          break;
        case BMAction.voidSale:
        // _bmposPlugin.voidSale('');voidSa
          if(!_isBind) {
            setState(() {
              _result = "Please click [Bind] First!";
            });
            return;
          }
          showTransactionInput(ctx, onOK: () {
            String transaction = _controller.text;
            param.putIfAbsent("trans_num", () => transaction);
            _bmposPlugin.voidSale(data: param);
          });
          break;
        case BMAction.cancel:
          _bmposPlugin.cancelTransaction();
          break;
        case BMAction.getPosInfor:
          _bmposPlugin.getPOSInfo(data: param);
          break;
      }
    } on Exception {
      _result = 'Failed to invoke method.';
    }
  }

  Map<String, dynamic> setParamForAction(String method) {
    Map<String, dynamic> param = {};
    switch (method) {
      case BMAction.sale:
        param = {"TransType": 1, "TransAmount": "000000000111", "timeOut": 30};
        break;
      case BMAction.voidSale:
        param = {"TransType": 101, "passWord": "123456"};
        break;
      case BMAction.settle:
        param = {"TransType": 21};
        break;
      case BMAction.setParam:
        param = {
          "PrimaryIP": TEST_IP1,
          "SecondaryIP": TEST_IP2,
          "PrimaryPort": TEST_PORT1,
          "SecondaryPort": TEST_PORT2,
          "passWord": "123456"
        };
        break;
    }
    return param;
  }

  void showTransactionInput(BuildContext ctx, {required Function() onOK}) {
    _controller.clear();
    showDialog<String>(
      context: ctx,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Void Sale'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text('Please input Transaction number'),
            TextField(
              autofocus: true,
              controller: _controller,
              maxLength: 6,
              maxLines: 1,
              keyboardType: TextInputType.number,
            )
          ],
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: (){
              Navigator.pop(context, 'Ok');
              onOK();
            },
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }
}

class MerchantAction {
  String title;
  String key;
  MerchantAction(this.title, this.key);
}
